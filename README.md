# parallelRan2

This is a parallel friendly implementation of the ran2 algorithm. In this
implementation the states are turned into a variable that is passed alongside
the seed throughout the program.

Compile using `gcc -fopenmp example.c -o example`
