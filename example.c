#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <omp.h>
#include "ran2.h"

int main ()
{
    //initializes the parallel block
    #pragma omp parallel
    {
        //defines the variable that will hold the state for the random number generator
        //The 1st field of the variable is the SEED, thats why is important that it is negative
        struct ran_state state = {-time(NULL)*(1+omp_get_thread_num()), 123456789, 0, {0} };
        printf("%d %li\n", omp_get_thread_num(), state.idum);

        //starts a loop where each value is run in a different thread
        #pragma omp for
        for (int i=0;i<=100;i++)
        {
            // prints the random numbers info
            printf("%d [%d] %f\n", i, omp_get_thread_num(), ran2(&state));
        }
    }
    return 0;
}
